
ifeq ($(wildcard bed),bed)
LUA     := $(CURDIR)/bed/bin/lua
LUACHECK:= $(CURDIR)/bed/bin/luacheck
LUAROCKS:= $(CURDIR)/bed/bin/luarocks
LUACOV  := $(CURDIR)/bed/bin/luacov
LUACOV_C:= $(CURDIR)/bed/bin/luacov-console
else
LUA     := lua
LUACHECK:= luacheck
LUAROCKS:= luarocks
LUACOV  := luacov
LUACOV_C:= luacov-console
endif
VERSION := $(shell LUA_PATH=";;src/?.lua" $(LUA) -e "m = require [[Silva]]; print(m._VERSION)")
TARBALL := lua-silva-$(VERSION).tar.gz
REV     := 1

LUAVER  := 5.3
PREFIX  := /usr/local
DPREFIX := $(DESTDIR)$(PREFIX)
LIBDIR  := $(DPREFIX)/share/lua/$(LUAVER)
INSTALL := install

BED_OPTS:= --lua latest

all:
	@echo "Nothing to build here, you can just make install"

install:
	$(INSTALL) -m 644 -D src/Silva.lua                  $(LIBDIR)/Silva.lua
	$(INSTALL) -m 644 -D src/Silva/identity.lua         $(LIBDIR)/Silva/identity.lua
	$(INSTALL) -m 644 -D src/Silva/lua.lua              $(LIBDIR)/Silva/lua.lua
	$(INSTALL) -m 644 -D src/Silva/pcre.lua             $(LIBDIR)/Silva/pcre.lua
	$(INSTALL) -m 644 -D src/Silva/shell.lua            $(LIBDIR)/Silva/shell.lua
	$(INSTALL) -m 644 -D src/Silva/template.lua         $(LIBDIR)/Silva/template.lua

uninstall:
	rm -f $(LIBDIR)/Silva.lua
	rm -f $(LIBDIR)/Silva/*.lua

manifest_pl := \
use strict; \
use warnings; \
my @files = qw{MANIFEST}; \
while (<>) { \
    chomp; \
    next if m{^\.git}; \
    next if m{^debian/}; \
    next if m{^rockspec/}; \
    push @files, $$_; \
} \
print join qq{\n}, sort @files;

rockspec_pl := \
use strict; \
use warnings; \
use Digest::MD5; \
open my $$FH, q{<}, q{$(TARBALL)} \
    or die qq{Cannot open $(TARBALL) ($$!)}; \
binmode $$FH; \
my %config = ( \
    version => q{$(VERSION)}, \
    rev     => q{$(REV)}, \
    md5     => Digest::MD5->new->addfile($$FH)->hexdigest(), \
); \
close $$FH; \
while (<>) { \
    s{@(\w+)@}{$$config{$$1}}g; \
    print; \
}

version:
	@echo $(VERSION)

CHANGES: dist.info
	perl -i.bak -pe "s{^$(VERSION).*}{q{$(VERSION)  }.localtime()}e" CHANGES

dist.info:
	perl -i.bak -pe "s{^version.*}{version = \"$(VERSION)\"}" dist.info

tag:
	git tag -a -m 'tag release $(VERSION)' $(VERSION)

MANIFEST:
	git ls-files | perl -e '$(manifest_pl)' > MANIFEST

$(TARBALL): MANIFEST
	[ -d lua-Silva-$(VERSION) ] || ln -s . lua-Silva-$(VERSION)
	perl -ne 'print qq{lua-Silva-$(VERSION)/$$_};' MANIFEST | \
	    tar -zc -T - -f $(TARBALL)
	rm lua-Silva-$(VERSION)

dist: $(TARBALL)

rockspec: $(TARBALL)
	perl -e '$(rockspec_pl)' rockspec.in      > rockspec/lua-silva-$(VERSION)-$(REV).rockspec

rock:
	$(LUAROCKS) pack rockspec/lua-silva-$(VERSION)-$(REV).rockspec

deb:
	echo "lua-silva ($(shell git describe --dirty)) unstable; urgency=medium" >  debian/changelog
	echo ""                         >> debian/changelog
	echo "  * UNRELEASED"           >> debian/changelog
	echo ""                         >> debian/changelog
	echo " -- $(shell git config --get user.name) <$(shell git config --get user.email)>  $(shell date -R)" >> debian/changelog
	fakeroot debian/rules clean binary

bed:
	hererocks bed $(BED_OPTS) --no-readline --luarocks latest --verbose
	bed/bin/luarocks install lua-testassertion
	bed/bin/luarocks install lrexlib-pcre
	bed/bin/luarocks install luafilesystem
	bed/bin/luarocks install luaposix
	bed/bin/luarocks install lunix
	bed/bin/luarocks install luacheck
	bed/bin/luarocks install luacov
	bed/bin/luarocks install luacov-console
	bed/bin/luarocks install luacov-reporter-lcov
	hererocks bed --show
	bed/bin/luarocks list

check: test

test:
	LUA_PATH="$(CURDIR)/src/?.lua;;" \
		prove --exec=$(LUA) test/*.lua

luacheck:
	-$(LUACHECK) --std=max --codes src --ignore 211/_ENV 542
	$(LUACHECK) --std=min --config .test.luacheckrc test

coverage:
	rm -f luacov.*
	-LUA_PATH="$(CURDIR)/src/?.lua;;" \
		prove --exec="$(LUA) -lluacov" test/*.lua
	$(LUACOV_C) $(CURDIR)/src
	$(LUACOV_C) -s $(CURDIR)/src
	$(LUACOV_C) test
	$(LUACOV_C) -s test

lcov:
	$(LUACOV) -r lcov
	genhtml luacov.report.out -o public/

README.html: README.md
	Markdown.pl README.md > README.html

pages:
	mkdocs build -d public

clean:
	rm -f MANIFEST *.bak luacov.* *.rockspec README.html

realclean: clean
	rm -rf bed

.PHONY: test rockspec deb CHANGES dist.info

