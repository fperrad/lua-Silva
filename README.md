
lua-Silva : your personal string matching expert
================================================

Introduction
------------

lua-Silva allows to match a URI against various kind of pattern :
URI Template, shell, Lua regex, PCRE regex, ...

Some of them allow to capture parts of URI.

lua-Silva was inspired by [Mustermann](http://sinatrarb.com/mustermann/)
(a part of Sinatra / Ruby).

Links
-----

The homepage is at <https://fperrad.frama.io/lua-Silva>,
and the sources are hosted at <https://framagit.org/fperrad/lua-Silva>.

Copyright and License
---------------------

Copyright (c) 2017-2024 Francois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.

