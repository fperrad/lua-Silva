
# Silva.template

---

# Manual

`Silva.template` implements an URI Template ([RFC 6570](https://tools.ietf.org/html/rfc6570) - level 3)
matching engine with capture.

All operator defined in the RFC 6570 are supported:

- `{var}`  simple string expansion
- `{+var}` reserved character string expansion
- `{#var}` fragment expansion, crosshatch-prefixed
- `{.var}` label expansion, dot-prefixed
- `{/var}` path segments, slash-prefixed
- `{;var}` path-style parameters, semicolon-prefixed
- `{?var}` form-style query, ampersand-separated
- `{&var}` form-style query continuation

The level 3 implies that multiple variables per expression are supported for all operator.
Each variable are separated by a comma, for example: `{var1,var2,var3}`.

The values modifiers (prefix modifier `:` and composite value `*`) defined in level 4 are not allowed.

The semicolon-prefixed path parameters and the form-style query parameters allow to match `name=value` pairs.
All expected name must be listed in the expression, an undeclared name causes a _no match_ result.
As extension, this strict control could be avoided, by using `*` as name.

The pct-encoded characters (for example `%20`) are handled in the pattern and in the uri to match.

# Examples

```lua
local uri = require 'Silva.template'

local matcher = uri('/index.html')
local capture = matcher('/index.html')
print(capture)  --> /index.html

-- level 1
local matcher = uri('/foo/{var}')
local capture = matcher('/foo/bar')
print(capture.var)  --> bar

-- level 2
local matcher = uri('/{+path}.js')
local capture = matcher('/foo/bar.js')
print(capture.path)  --> foo/bar

-- level 3
local matcher = uri('{/path1,path2}123{?query,number}')
local capture = matcher('/foo/bar123?number=42&query=baz')
print(capture.path1, capture.path2, capture.number, capture.query)   --> foo   bar   42    baz

-- extension
local matcher = uri('{/path}/bar{?*}')
local capture = matcher('/foo/bar?x=1&y=2&z=3')
print(capture.path, capture.x, capture.y, capture.z)  --> foo   1     2     3
```
