codes = true
read_globals = {
    -- Test.More
    'plan',
    'done_testing',
    'skip_all',
    'BAIL_OUT',
    'subtest',
    'diag',
    'note',
    'skip',
    'todo_skip',
    'skip_rest',
    'todo',
    -- Test.Assertion
    'equals',
    'array_equals',
    'is_function',
    'is_string',
    'is_table',
    'falsy',
    'truthy',
    'matches',
    'error_matches',
    'require_ok',
}
