#!/usr/bin/env lua

require 'Test.Assertion'

plan(4)

local ctor = require 'Silva.identity'

is_function( ctor )

local sme0 = ctor'/foo/bar'
truthy( sme0('/foo/bar'), 'same string' )
falsy( sme0('/foo/baz') )
falsy( sme0('/foo/bar/baz') )
