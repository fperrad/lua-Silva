#!/usr/bin/env lua

require 'Test.Assertion'

plan(166)

local ctor = require 'Silva.shell'
local sme0, sme1

sme0 = ctor'foo[1234]bar'
truthy( sme0, 'foo[1234]bar' )
falsy(  sme0'foo0bar' )
truthy( sme0'foo1bar' )
truthy( sme0'foo2bar' )
truthy( sme0'foo3bar' )
truthy( sme0'foo4bar' )
falsy(  sme0'foo5bar' )
sme1 = ctor'foo[^1234]bar'
truthy( sme1, 'foo[^1234]bar' )
truthy( sme1'foo0bar' )
falsy(  sme1'foo1bar' )
falsy(  sme1'foo2bar' )
falsy(  sme1'foo3bar' )
falsy(  sme1'foo4bar' )
truthy( sme1'foo5bar' )

sme0 = ctor'foo[1-4]bar'
truthy( sme0, 'foo[1-4]bar' )
falsy(  sme0'foo0bar' )
truthy( sme0'foo1bar' )
truthy( sme0'foo2bar' )
truthy( sme0'foo3bar' )
truthy( sme0'foo4bar' )
falsy(  sme0'foo5bar' )
sme1 = ctor'foo[^1-4]bar'
truthy( sme1, 'foo[^1-4]bar' )
truthy( sme1'foo0bar' )
falsy(  sme1'foo1bar' )
falsy(  sme1'foo2bar' )
falsy(  sme1'foo3bar' )
falsy(  sme1'foo4bar' )
truthy( sme1'foo5bar' )

sme0 = ctor'foo[12-4]bar'
truthy( sme0, 'foo[12-4]bar' )
falsy(  sme0'foo0bar' )
truthy( sme0'foo1bar' )
truthy( sme0'foo2bar' )
truthy( sme0'foo3bar' )
truthy( sme0'foo4bar' )
falsy(  sme0'foo5bar' )
sme1 = ctor'foo[^12-4]bar'
truthy( sme1, 'foo[^12-4]bar' )
truthy( sme1'foo0bar' )
falsy(  sme1'foo1bar' )
falsy(  sme1'foo2bar' )
falsy(  sme1'foo3bar' )
falsy(  sme1'foo4bar' )
truthy( sme1'foo5bar' )

sme0 = ctor'foo[123-4]bar'
truthy( sme0, 'foo[123-4]bar' )
falsy(  sme0'foo0bar' )
truthy( sme0'foo1bar' )
truthy( sme0'foo2bar' )
truthy( sme0'foo3bar' )
truthy( sme0'foo4bar' )
falsy(  sme0'foo5bar' )
sme1 = ctor'foo[^123-4]bar'
truthy( sme1, 'foo[^123-4]bar' )
truthy( sme1'foo0bar' )
falsy(  sme1'foo1bar' )
falsy(  sme1'foo2bar' )
falsy(  sme1'foo3bar' )
falsy(  sme1'foo4bar' )
truthy( sme1'foo5bar' )

sme0 = ctor'foo[12-34]bar'
truthy( sme0, 'foo[12-34]bar' )
falsy(  sme0'foo0bar' )
truthy( sme0'foo1bar' )
truthy( sme0'foo2bar' )
truthy( sme0'foo3bar' )
truthy( sme0'foo4bar' )
falsy(  sme0'foo5bar' )
sme1 = ctor'foo[^12-34]bar'
truthy( sme1, 'foo[^12-34]bar' )
truthy( sme1'foo0bar' )
falsy(  sme1'foo1bar' )
falsy(  sme1'foo2bar' )
falsy(  sme1'foo3bar' )
falsy(  sme1'foo4bar' )
truthy( sme1'foo5bar' )

sme0 = ctor'foo[1-234]bar'
truthy( sme0, 'foo[1-234]bar' )
falsy(  sme0'foo0bar' )
truthy( sme0'foo1bar' )
truthy( sme0'foo2bar' )
truthy( sme0'foo3bar' )
truthy( sme0'foo4bar' )
falsy(  sme0'foo5bar' )
sme1 = ctor'foo[^1-234]bar'
truthy( sme1, 'foo[^1-234]bar' )
truthy( sme1'foo0bar' )
falsy(  sme1'foo1bar' )
falsy(  sme1'foo2bar' )
falsy(  sme1'foo3bar' )
falsy(  sme1'foo4bar' )
truthy( sme1'foo5bar' )

sme0 = ctor'foo[1-34]bar'
truthy( sme0, 'foo[1-34]bar' )
falsy(  sme0'foo0bar' )
truthy( sme0'foo1bar' )
truthy( sme0'foo2bar' )
truthy( sme0'foo3bar' )
truthy( sme0'foo4bar' )
falsy(  sme0'foo5bar' )
sme1 = ctor'foo[^1-34]bar'
truthy( sme1, 'foo[1234]bar' )
truthy( sme1'foo0bar' )
falsy(  sme1'foo1bar' )
falsy(  sme1'foo2bar' )
falsy(  sme1'foo3bar' )
falsy(  sme1'foo4bar' )
truthy( sme1'foo5bar' )

sme0 = ctor'foo[1-23-4]bar'
truthy( sme0, 'foo[1-23-4]bar' )
falsy(  sme0'foo0bar' )
truthy( sme0'foo1bar' )
truthy( sme0'foo2bar' )
truthy( sme0'foo3bar' )
truthy( sme0'foo4bar' )
falsy(  sme0'foo5bar' )
sme1 = ctor'foo[^1-23-4]bar'
truthy( sme1, 'foo[^1-23-4]bar' )
truthy( sme1'foo0bar' )
falsy(  sme1'foo1bar' )
falsy(  sme1'foo2bar' )
falsy(  sme1'foo3bar' )
falsy(  sme1'foo4bar' )
truthy( sme1'foo5bar' )

sme0 = ctor'foo[_]bar'
truthy( sme0, 'foo[_]bar' )
falsy(  sme0'foo bar' )
truthy( sme0'foo_bar' )
sme1 = ctor'foo[^_]bar'
truthy( sme1, 'foo[^_]bar' )
truthy( sme1'foo bar' )
falsy(  sme1'foo_bar' )

sme0 = ctor'foo[_-]bar'
truthy( sme0, 'foo[_-]bar' )
falsy(  sme0'foo bar' )
truthy( sme0'foo-bar' )
truthy( sme0'foo_bar' )
sme1 = ctor'foo[^_-]bar'
truthy( sme1, 'foo[^_-]bar' )
truthy( sme1'foo bar' )
falsy(  sme1'foo-bar' )
falsy(  sme1'foo_bar' )

sme0 = ctor'foo[-]bar'
truthy( sme0, 'foo[-]bar' )
falsy(  sme0'foo bar' )
truthy( sme0'foo-bar' )
sme1 = ctor'foo[^-]bar'
truthy( sme1, 'foo[^-]bar' )
truthy( sme1'foo bar' )
falsy(  sme1'foo-bar' )

sme0 = ctor'foo[-_]bar'
truthy( sme0, 'foo[-_]bar' )
falsy(  sme0'foo bar' )
truthy( sme0'foo-bar' )
truthy( sme0'foo_bar' )
sme1 = ctor'foo[^_-]bar'
truthy( sme1, 'foo[^_-]bar' )
truthy( sme1'foo bar' )
falsy(  sme1'foo-bar' )
falsy(  sme1'foo_bar' )

sme0 = ctor'foo[_,/]bar'
truthy( sme0, 'foo[_,/]bar' )
falsy(  sme0'foo bar' )
truthy( sme0'foo_bar' )
falsy(  sme0'foo/bar' )
sme1 = ctor'foo[^_,/]bar'
truthy( sme1, 'foo[^_,/]bar' )
truthy( sme1'foo bar' )
falsy(  sme1'foo_bar' )
falsy(  sme1'foo/bar' )

sme0 = ctor'foo['
truthy( sme0, 'foo[' )
falsy(  sme0'foo ' )
sme1 = ctor'foo[^'
truthy( sme1, 'foo[^' )
falsy(  sme1'foo ' )

sme0 = ctor'foo[_'
truthy( sme0, 'foo[_' )
falsy(  sme0'foo_' )
sme1 = ctor'foo[^_'
truthy( sme1, 'foo[^_' )
falsy(  sme1'foo_' )

sme0 = ctor'foo[_-'
truthy( sme0, 'foo[_-' )
falsy(  sme0'foo_' )
sme1 = ctor'foo[^_-'
truthy( sme1, 'foo[^_-' )
falsy(  sme1'foo_' )

sme0 = ctor'foo[.]c'
truthy( sme0, 'foo[.]c' )
truthy( sme0'foo.c' )
falsy(  sme0'foo:c' )
falsy(  sme0'foo' )

sme0 = ctor'[.]foo'
truthy( sme0, '[.]foo' )
falsy(  sme0'.foo' )
