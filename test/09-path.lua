#!/usr/bin/env lua

require 'Test.Assertion'

plan(29)

local re = require 'Silva.lua'
local uri = require 'Silva.template'

local sme0 = uri('/foo{+path}{.ext}')   -- both captures are greedy, and the second could succeed with empty
local t = sme0('/foo/index.html')
is_table( t )
equals( t.path, '/index.html' )
equals( t.ext, nil )

local sme1 = re'^/foo(.-)%.(%w+)$'      -- the first capture is non greedy
t = sme1('/foo/index.html')
is_table( t )
equals( t[1], '/index' )
equals( t[2], 'html' )


sme0 = uri('{+path}.html')
truthy( sme0('/foo/index.html') )
truthy( sme0('bar.html') )
falsy( sme0('baz.htm') )

sme1 = re('%.html$')
truthy( sme1('/foo/index.html') )
truthy( sme1('bar.html') )
falsy( sme1('baz.htm') )

sme0 = uri('/index.html')
truthy( sme0('/index.html') )
falsy( sme0('/index.htm') )

--
--  level 4 of URI Template is not implemented
--  example of prefix value: /foo{dir:1,dir:2,dir,file}
--
error_matches( function () uri('/foo{dir:1,dir:2,dir,file}') end,
        "modifier %(level 4%) found at position 9" )

local sme2 = uri('/foo{/dir_1,dir_2,dir,file}')
t = sme2('/foo/S/SI/SILVA/bar.txt')
is_table( t )
equals( t.dir_1, 'S' )
equals( t.dir_2, 'SI' )
equals( t.dir, 'SILVA' )
equals( t.file, 'bar.txt' )
truthy( sme2('/foo/S/AB/SILVA/bar.txt') )
truthy( sme2('/foo/S/SI/ABCDE/bar.txt') )

local sme3 = re('^/foo/(%w)/(%1%w)/(%2%w*)/(.*)$')
t = sme3('/foo/S/SI/SILVA/bar.txt')
is_table( t )
equals( t[1], 'S' )
equals( t[2], 'SI' )
equals( t[3], 'SILVA' )
equals( t[4], 'bar.txt' )
falsy( sme3('/foo/S/AB/SILVA/bar.txt') )
falsy( sme3('/foo/S/SI/ABCDE/bar.txt') )
