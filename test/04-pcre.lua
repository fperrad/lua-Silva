#!/usr/bin/env lua

require 'Test.Assertion'

if not pcall(require, 'rex_pcre') then
    skip_all 'no rex_pcre'
end

plan(7)

local ctor = require 'Silva.pcre'

is_function( ctor )

local sme0 = ctor'^hello'
local sme1 = ctor'^(h.ll.)'
local sme2 = ctor'^(h.)l(l.)'

truthy( sme0('hello world') )
falsy( sme0('Hello world') )

array_equals( sme1('hello world'), {'hello'})
falsy( sme1('Hello world') )

array_equals( sme2('hello world'), {'he', 'lo'})
falsy( sme2('Hello world') )
