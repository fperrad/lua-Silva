#!/usr/bin/env lua

require 'Test.Assertion'

plan(8)

local ctor = require 'Silva.lua'

is_function( ctor )

local sme0 = ctor'^hello'
local sme1 = ctor'^(h.ll.)'
local sme2 = ctor'^(h.)l(l.)'

truthy( sme0('hello') )
truthy( sme0('hello world') )
falsy( sme0('Hello world') )

array_equals( sme1('hello world'), {'hello'})
falsy( sme1('Hello world') )

array_equals( sme2('hello world'), {'he', 'lo'})
falsy( sme2('Hello world') )
