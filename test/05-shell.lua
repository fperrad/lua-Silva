#!/usr/bin/env lua

require 'Test.Assertion'

plan(40)

local ctor = require 'Silva.shell'

is_function( ctor )

local sme0 = ctor'foo.c'
truthy( sme0, 'foo.c' )
truthy( sme0'foo.c' )
falsy(  sme0'foo.cpp' )
falsy(  sme0'foo.h' )
falsy(  sme0'foo' )
falsy(  sme0'bar' )

local sme1 = ctor'foo?bar'
truthy( sme1, 'foo?bar' )
truthy( sme1'foo0bar' )
falsy(  sme1'foo0baz' )
falsy(  sme1'foo' )
falsy(  sme1'foo/bar' )

local sme2 = ctor'foo???baz'
truthy( sme2, 'foo???baz' )
truthy( sme2'foobarbaz' )
falsy(  sme2'foobarbar' )

local sme3 = ctor'?foo'
truthy( sme3, '?foo' )
truthy( sme3'0foo' )
falsy(  sme3'.foo' )

local sme4 = ctor'foo*'
truthy( sme4, 'foo*' )
truthy( sme4'foo' )
truthy( sme4'foo0' )
truthy( sme4'foo00' )
truthy( sme4'foo000' )
truthy( sme4'foo/000' )

local sme5 = ctor'foo*?'
truthy( sme5, 'foo*?' )
falsy(  sme5'foo' )
truthy( sme5'foo0' )
truthy( sme5'foo00' )
truthy( sme5'foo000' )
truthy( sme5'foo.000' )
falsy(  sme5'foo/000' )

local sme6 = ctor'*.c'
truthy( sme6, '*.c' )
truthy( sme6'foo.c' )
falsy(  sme6'foo.cpp' )
falsy(  sme6'foo.h' )
falsy(  sme6'.foo.c' )

local sme7 = ctor'foo/*/*.t'
truthy( sme7, 'foo/*/*.t' )
truthy( sme7'foo/bar/baz.t' )
falsy(  sme7'foo/bak/.baz.t' )
falsy(  sme7'foo/.bak/baz.t' )
