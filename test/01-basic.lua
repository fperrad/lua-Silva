#!/usr/bin/env lua

require 'Test.Assertion'

plan(5)

local sme = require 'Silva'

local sme0 = sme('/foo/bar', 'identity')
truthy( sme0('/foo/bar'), 'same string' )
falsy( sme0('/foo/baz') )

local sme1 = sme'/foo/{var}'
local t = sme1('/foo/bar')
is_table( t )
equals( t.var, 'bar' )
falsy( sme1('/bar/baz') )

