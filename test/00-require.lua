#!/usr/bin/env lua

require 'Test.Assertion'

plan(10)

if not require_ok 'Silva' then
    BAIL_OUT "no lib"
end

local m = require 'Silva'
is_table( m )
equals( m, package.loaded.Silva )

is_function( m.matcher, 'matcher' )
is_function( m.array_matcher, 'array_matcher' )

equals( m._NAME, 'Silva', "_NAME" )
matches( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
matches( m._DESCRIPTION, 'string matching expert', "_DESCRIPTION" )
is_string( m._VERSION, "_VERSION" )
matches( m._VERSION, '^%d%.%d%.%d$' )

