#!/usr/bin/env lua

require 'Test.Assertion'

local tb = require 'Test.Builder'.new()
local function kv_equals (t, k, expected, name)
    if type(t) ~= 'table' then
        tb:ok(false, name)
        tb:diag("    " .. tostring(t) .. " isn't a 'table' it's a '" .. type(t) .. "'")
    else
        local got = t[k]
        local pass = got == expected
        tb:ok(pass, name)
        if not pass then
            tb:diag("         got: " .. tostring(got)
               .. "\n    expected: " .. tostring(expected))
        end
    end
end

plan(274)

local ctor = require 'Silva.template'

is_function( ctor )
local sme, capture

sme = ctor'/foo/bar'
equals( sme('/foo/bar'), '/foo/bar', 'same string' )
falsy( sme('/foo/baz') )
falsy( sme('/foo/bar/baz') )

sme = ctor'/'
equals( sme('/'), '/', 'same string' )
falsy( sme('/foo') )

sme = ctor''
equals( sme(''), '', 'empty string' )
falsy( sme('/foo') )

-- level 1
sme = ctor'/foo/{var}'
capture = sme('/foo/bar')
kv_equals( capture, 'var', 'bar' )
capture = sme('/foo/Hello%20World%21')
kv_equals( capture, 'var', 'Hello World!' )
capture = sme('/foo/')
kv_equals( capture, 'var', '' )
falsy( sme('/foo/bar/baz') )
falsy( sme('/bar/baz') )

sme = ctor'/foo/0{var}/baz'
capture = sme('/foo/0bar/baz')
kv_equals( capture, 'var', 'bar' )
capture = sme('/foo/%30%62%61%7a/%62%61%7a')
kv_equals( capture, 'var', 'baz' )
capture = sme('/foo/0/baz')
kv_equals( capture, 'var', '' )
falsy( sme('/foo/0bar/bar') )

sme = ctor'/foo/{var}123/baz'
capture = sme('/foo/bar123/baz')
kv_equals( capture, 'var', 'bar' )
capture = sme('/foo/123/baz')
kv_equals( capture, 'var', '' )
falsy( sme('/foo/bar0/baz') )
falsy( sme('/foo/bar123/bar') )

sme = ctor'/foo/{bar}/{%62%61r}'
capture = sme('/foo/bar/%62%61r')
kv_equals( capture, 'bar', 'bar' )
kv_equals( capture, '%62%61r', 'bar' )
capture = sme('/foo/%62%61r/bar')
kv_equals( capture, 'bar', 'bar' )
kv_equals( capture, '%62%61r', 'bar' )

-- level 2
sme = ctor'/foo/{+var}'
capture = sme('/foo/bar')
kv_equals( capture, 'var', 'bar' )
capture = sme('/foo/bar/baz')
kv_equals( capture, 'var', 'bar/baz' )
capture = sme('/foo/')
kv_equals( capture, 'var', '' )

sme = ctor'/foo/{+var}/here'
capture = sme('/foo/bar/baz/here')
kv_equals( capture, 'var', 'bar/baz' )

sme = ctor'/foo/{+var}123'
capture = sme('/foo/bar123')
kv_equals( capture, 'var', 'bar' )
capture = sme('/foo/bar/baz123')
kv_equals( capture, 'var', 'bar/baz' )
capture = sme('/foo/123')
kv_equals( capture, 'var', '' )

sme = ctor'here?ref={+var}'
capture = sme('here?ref=/foo/bar')
kv_equals( capture, 'var', '/foo/bar' )

sme = ctor'/foo{#var}'
capture = sme('/foo#bar')
kv_equals( capture, 'var', 'bar' )
capture = sme('/foo#Hello%20World%21')
kv_equals( capture, 'var', 'Hello World!' )
capture = sme('/foo#')
kv_equals( capture, 'var', '' )
capture = sme('/foo')
kv_equals( capture, 'var', nil )
falsy( sme('/foo/') )
falsy( sme('/bar#baz') )

sme = ctor'/foo{#var}123'
capture = sme('/foo#bar123')
kv_equals( capture, 'var', 'bar' )
falsy( sme('/foo#bar0') )

-- level 3
sme = ctor'map?{x,y}'
capture = sme('map?1024,768')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
capture = sme('map?1024')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', nil )
capture = sme('map?1024,')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '' )
falsy( sme('map?1,2,3') )
capture = sme('map?')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', nil )
capture = sme('map?,')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )

sme = ctor'/foo/{+path,x}/here'
capture = sme('/foo/bar/baz,1024/here')
kv_equals( capture, 'path', 'bar/baz' )
kv_equals( capture, 'x', '1024' )
capture = sme('/foo/bar/baz/here')
kv_equals( capture, 'path', 'bar/baz' )
kv_equals( capture, 'x', nil )
capture = sme('/foo/bar/baz,/here')
kv_equals( capture, 'path', 'bar/baz' )
kv_equals( capture, 'x', '' )

sme = ctor'/foo{#path,x}/here'
capture = sme('/foo#bar,1024/here')
kv_equals( capture, 'path', 'bar' )
kv_equals( capture, 'x', '1024' )
capture = sme('/foo#bar/here')
kv_equals( capture, 'path', 'bar' )
kv_equals( capture, 'x', nil )
capture = sme('/foo#bar,/here')
kv_equals( capture, 'path', 'bar' )
kv_equals( capture, 'x', '' )

sme = ctor'/foo{.var}'
capture = sme('/foo.txt')
kv_equals( capture, 'var', 'txt' )
capture = sme('/foo.')
kv_equals( capture, 'var', '' )
capture = sme('/foo')
kv_equals( capture, 'var', nil )

sme = ctor'/foo{.x,y}'
capture = sme('/foo.1024.768')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
capture = sme('/foo.1024')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', nil )
capture = sme('/foo.1024.')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '' )

sme = ctor'/foo{.x}{.y}'
capture = sme('/foo.1024.768')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
capture = sme('/foo.1024')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', nil )
capture = sme('/foo.1024.')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '' )

sme = ctor'/foo{.x,y}bar'
capture = sme('/foo.1024.768bar')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
capture = sme('/foo.1024bar')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', nil )
capture = sme('/foo.1024.bar')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '' )

sme = ctor'/foo{.x}{.y}bar'
capture = sme('/foo.1024.768bar')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
capture = sme('/foo.1024bar')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', nil )
capture = sme('/foo.1024.bar')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '' )

sme = ctor'/foo{/var}'
capture = sme('/foo/bar')
kv_equals( capture, 'var', 'bar' )
capture = sme('/foo/')
kv_equals( capture, 'var', '' )
capture = sme('/foo')
kv_equals( capture, 'var', nil )

sme = ctor'/foo{/x,y}123'
capture = sme('/foo/bar/baz123')
kv_equals( capture, 'x', 'bar' )
kv_equals( capture, 'y', 'baz' )
capture = sme('/foo/bar123')
kv_equals( capture, 'x', 'bar' )
kv_equals( capture, 'y', nil )
capture = sme('/foo/bar/123')
kv_equals( capture, 'x', 'bar' )
kv_equals( capture, 'y', '' )
capture = sme('/foo123')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
capture = sme('/foo/123')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', nil )

sme = ctor'/foo{/x}{/y}123'
capture = sme('/foo/bar/baz123')
kv_equals( capture, 'x', 'bar' )
kv_equals( capture, 'y', 'baz' )
capture = sme('/foo/bar123')
kv_equals( capture, 'x', 'bar' )
kv_equals( capture, 'y', nil )
capture = sme('/foo/bar/123')
kv_equals( capture, 'x', 'bar' )
kv_equals( capture, 'y', '' )
capture = sme('/foo123')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
capture = sme('/foo/123')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', nil )

sme = ctor'/foo{/var,x}/here'
capture = sme('/foo/bar/1024/here')
kv_equals( capture, 'var', 'bar' )
kv_equals( capture, 'x', '1024' )
capture = sme('/foo/bar//here')
kv_equals( capture, 'var', 'bar' )
kv_equals( capture, 'x', '' )

sme = ctor'/foo{/var}{/x}/here'
capture = sme('/foo/bar/1024/here')
kv_equals( capture, 'var', 'bar' )
kv_equals( capture, 'x', '1024' )
capture = sme('/foo/bar//here')
kv_equals( capture, 'var', 'bar' )
kv_equals( capture, 'x', '' )

sme = ctor'/foo{;x,y}'
capture = sme('/foo;x=1024;y=768')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
capture = sme('/foo;x;y')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )
falsy( sme('/foo;x=1024;y=768;z=0') )
capture = sme('/foo')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
capture = sme('/foo;')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
falsy( sme('/foo;z=0') )

sme = ctor'/foo{;x,y}/here'
capture = sme('/foo;x=1024;y=768/here')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
capture = sme('/foo;x;y/here')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )
falsy( sme('/foo;x=1024;y=768;z=0/here') )
capture = sme('/foo/here')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
capture = sme('/foo;/here')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
falsy( sme('/foo;z=0/here') )

sme = ctor'/foo{;x,y}bar/here'
capture = sme('/foo;x=1024;y=768bar/here')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
falsy( sme('/foo;x;ybar/here') )
capture = sme('/foobar/here')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
falsy( sme('/foo;bar/here') )
falsy( sme('/foo;z=0bar/here') )

sme = ctor'/foo{;x,y}?fixed'
capture = sme('/foo;x=1024;y=768?fixed')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
capture = sme('/foo;x;y?fixed')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )
falsy( sme('/foo;x=1024;y=768;z=0?fixed') )
capture = sme('/foo?fixed')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
capture = sme('/foo;?fixed')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
falsy( sme('/foo;z=0?fixed') )

sme = ctor'/foo{;x,y}#fixed'
capture = sme('/foo;x=1024;y=768#fixed')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
capture = sme('/foo;x;y#fixed')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )
falsy( sme('/foo;x=1024;y=768;z=0#fixed') )
capture = sme('/foo#fixed')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
capture = sme('/foo;#fixed')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
falsy( sme('/foo;z=0#fixed') )

sme = ctor'/foo{?x,y}'
capture = sme('/foo?x=1024&y=768')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
capture = sme('/foo?x=&y=')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )
falsy( sme('/foo?x=1024&y=768&z=0') )
capture = sme('/foo')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
capture = sme('/foo?')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
falsy( sme('/foo?z=0') )

sme = ctor'/foo{?x,y}#fixed'
capture = sme('/foo?x=1024&y=768#fixed')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
capture = sme('/foo?x=&y=#fixed')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )
falsy( sme('/foo?x=1024&y=768&z=0#fixed') )
capture = sme('/foo#fixed')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
capture = sme('/foo?#fixed')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
falsy( sme('/foo?z=0#fixed') )

sme = ctor'/foo?fixed=yes{&x,y}'
capture = sme('/foo?fixed=yes&x=1024&y=768')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
capture = sme('/foo?fixed=yes&x=&y=')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )
falsy( sme('/foo?fixed=yes&x=1024&y=768&z=0') )
capture = sme('/foo?fixed=yes')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
capture = sme('/foo?fixed=yes&')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
falsy( sme('/foo?fixed=yes&z=0') )

sme = ctor'/foo?fixed=yes{&x,y}#fixed'
capture = sme('/foo?fixed=yes&x=1024&y=768#fixed')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
capture = sme('/foo?fixed=yes&x=&y=#fixed')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )
falsy( sme('/foo?fixed=yes&x=1024&y=768&z=0#fixed') )
capture = sme('/foo?fixed=yes#fixed')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
capture = sme('/foo?fixed=yes&#fixed')
kv_equals( capture, 'x', nil )
kv_equals( capture, 'y', nil )
falsy( sme('/foo?fixed=yes&z=0#fixed') )

-- x-www-form-urlencoded
sme = ctor'/foo/{path}{?query}{#frag}'
capture = sme('/foo/bar+baz')
kv_equals( capture, 'path', 'bar+baz' )
kv_equals( capture, 'query', nil )
kv_equals( capture, 'frag', nil )
capture = sme('/foo/bar+baz?query=foo+bar+baz')
kv_equals( capture, 'path', 'bar+baz' )
kv_equals( capture, 'query', 'foo bar baz' )
kv_equals( capture, 'frag', nil )
capture = sme('/foo/bar+baz?query=foo+bar+baz#baz+bar')
kv_equals( capture, 'path', 'bar+baz' )
kv_equals( capture, 'query', 'foo bar baz' )
kv_equals( capture, 'frag', 'baz+bar' )

sme = ctor'/foo/{path}?first=yes%20or%20no{&query}#foo{frag}'
capture = sme('/foo/bar+baz?first=yes+or+no#foo')
kv_equals( capture, 'path', 'bar+baz' )
kv_equals( capture, 'query', nil )
kv_equals( capture, 'frag', '' )
capture = sme('/foo/bar+baz?first=yes+or+no&query=foo+bar+baz#foo')
kv_equals( capture, 'path', 'bar+baz' )
kv_equals( capture, 'query', 'foo bar baz' )
kv_equals( capture, 'frag', '' )
capture = sme('/foo/bar+baz?first=yes+or+no&query=foo+bar+baz#foobaz+bar')
kv_equals( capture, 'path', 'bar+baz' )
kv_equals( capture, 'query', 'foo bar baz' )
kv_equals( capture, 'frag', 'baz+bar' )

-- extension
sme = ctor'/foo{;*}'
capture = sme('/foo;x=1024;y=768')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
capture = sme('/foo;x;y')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )

sme = ctor'/foo{;z,*}/here'
capture = sme('/foo;x=1024;y=768/here')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
kv_equals( capture, 'z', nil )
capture = sme('/foo;x;y/here')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )
kv_equals( capture, 'z', nil )

sme = ctor'/foo{;*,z}bar/here'
capture = sme('/foo;x=1024;y=768bar/here')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
kv_equals( capture, 'z', nil )

sme = ctor'/foo{;z,*}?fixed'
capture = sme('/foo;x=1024;y=768?fixed')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
kv_equals( capture, 'z', nil )
capture = sme('/foo;x;y?fixed')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )
kv_equals( capture, 'z', nil )

sme = ctor'/foo{;*,z}#fixed'
capture = sme('/foo;x=1024;y=768#fixed')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
kv_equals( capture, 'z', nil )
capture = sme('/foo;x;y#fixed')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )
kv_equals( capture, 'z', nil )

sme = ctor'/foo{?*}'
capture = sme('/foo?x=1024&y=768')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
capture = sme('/foo?x=&y=')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )

sme = ctor'/foo{?z,*}#fixed'
capture = sme('/foo?x=1024&y=768#fixed')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
kv_equals( capture, 'z', nil )
capture = sme('/foo?x=&y=#fixed')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )
kv_equals( capture, 'z', nil )

sme = ctor'/foo?fixed=yes{&*,z}'
capture = sme('/foo?fixed=yes&x=1024&y=768')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
kv_equals( capture, 'z', nil )
capture = sme('/foo?fixed=yes&x=&y=')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )
kv_equals( capture, 'z', nil )

sme = ctor'/foo?fixed=yes{&z,*}#fixed'
capture = sme('/foo?fixed=yes&x=1024&y=768#fixed')
kv_equals( capture, 'x', '1024' )
kv_equals( capture, 'y', '768' )
kv_equals( capture, 'z', nil )
capture = sme('/foo?fixed=yes&x=&y=#fixed')
kv_equals( capture, 'x', '' )
kv_equals( capture, 'y', '' )
kv_equals( capture, 'z', nil )
