#!/usr/bin/env lua

require 'Test.Assertion'

plan(41)

local ctor = require 'Silva.template'

is_function( ctor )

truthy( ctor'/foo/bar' )
truthy( ctor'/foo/%62%61%7a' )
truthy( ctor'/' )
truthy( ctor'' )
error_matches( function () ctor(true) end,
        "bad argument #1 to 'gmatch' %(string expected, got boolean%)" )

-- level 1
truthy( ctor'/foo/{var}' )
truthy( ctor'/foo/{var}/here' )
error_matches( function () ctor'/foo/{var-}/' end,
        "invalid character found at position 10" )
error_matches( function () ctor'/foo/{var%GG}/' end,
        "invalid triplet found at position 10" )
truthy( ctor'/foo/{bar}/{baz}' )
truthy( ctor'/foo/{var%20%31}' )
error_matches( function () ctor'/foo/{var}/{var}' end,
        "duplicated name var" )
truthy( ctor'/foo/{bar}/{%62%61r}' )

-- level 2
truthy( ctor'/foo/{+var}' )
truthy( ctor'/foo/{+var}/here' )
truthy( ctor'/foo/{#var}' )

-- level 3
truthy( ctor'/foo/{x,y}' )
truthy( ctor'/foo/{+x,y}' )
truthy( ctor'/foo/{#x,y}' )
truthy( ctor'/foo/{.x,y}' )
truthy( ctor'/foo/{/x,y}' )
truthy( ctor'/foo/{;x,y}' )
truthy( ctor'/foo/{?x,y}' )
truthy( ctor'/foo/{&x,y}' )
error_matches( function () ctor'/foo/{=var}/' end,
        "operator for future extension found at position 7" )
error_matches( function () ctor'/foo/{,var}/' end,
        "operator for future extension found at position 7" )
error_matches( function () ctor'/foo/{!var}/' end,
        "operator for future extension found at position 7" )
error_matches( function () ctor'/foo/{@var}/' end,
        "operator for future extension found at position 7" )
error_matches( function () ctor'/foo/{|var}/' end,
        "operator for future extension found at position 7" )
error_matches( function () ctor'/foo/{-var}/' end,
        "invalid character found at position 7" )
error_matches( function () ctor'/foo/{x,x,y}' end,
        "duplicated name x" )
error_matches( function () ctor'/foo/{x,y,x}' end,
        "duplicated name x" )

-- level 4
error_matches( function () ctor'/foo/{var:3}/' end,
        "modifier %(level 4%) found at position 10" )
error_matches( function () ctor'/foo/{var*}/' end,
        "modifier %(level 4%) found at position 10" )

-- extension
truthy( ctor'/foo/{;*}' )
truthy( ctor'/foo/{?*}' )
truthy( ctor'/foo/{&*}' )
error_matches( function () ctor'/foo/{;*var}/' end,
        "invalid name %*var" )
error_matches( function () ctor'/foo/{?*var}/' end,
        "invalid name %*var" )
error_matches( function () ctor'/foo/{&*var}/' end,
        "invalid name %*var" )
